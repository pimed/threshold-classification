# Working with OpenCV 4.5.3.56
import argparse
import cv2 as cv
import matplotlib.pylab as plt
a = 0
b = 255
c = 55
d = 141

img = cv.imread("./src/wom2.png",cv.IMREAD_GRAYSCALE)
imgBase = cv.imread("./src/wom2.png")
print(type(imgBase))
print(img.dtype)
size = img.shape

for i in range(size[0]):
    for j in range(size[1]):
        img[i, j] = (img[i,j] - c)*((b-a)/(d-c)) + a

cv.imshow("Imagen con contrast stretching", img)
cv.imshow("Imagen original", imgBase)

#plt.hist(imgBase.ravel(), 256, [0, 256])
#plt.show()
cv.waitKey(0)
cv.destroyAllWindows()