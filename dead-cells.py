# Working with OpenCV 4.5.3.56
import cv2 as cv
import matplotlib.pylab as plt

#LOADING THE IMAGE
imgBase = cv.imread("./src/nervous-tissue2.PNG")
img = cv.imread("./src/nervous-tissue2.PNG")
size = img.shape

#HISTOGRAM OF THE IMAGE
#plt.hist(img.ravel(), 256, [0, 256])
#plt.show()

for i in range(size[0]):
    for j in range(size[1]):
        k = img[i, j]
        if(k[0] > 160 and k[1] > 160 and k[2] > 160):
            img[i, j] = [200, 200, 200]

cv.imshow("Tejido nervioso", img)
cv.imshow("Tejido nervioso base", imgBase)
cv.waitKey(0)
cv.destroyAllWindows()
