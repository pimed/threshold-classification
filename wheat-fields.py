# Working with OpenCV 4.5.3.56
import cv2 as cv
import matplotlib.pylab as plt

#HISTOGRAM OF THE IMAGE
img = cv.imread("./src/wheat-fields.PNG", cv.IMREAD_GRAYSCALE)
imgBase = cv.imread("./src/wheat-fields.PNG")
size = img.shape

for i in range(size[0]):
    for j in range(size[1]):
        k = img[i, j]
        if(k > 178 and k < 200):
            img[i, j] = 255
        else:
            img[i, j] = 0
        

cv.imshow("Campos de trigo", img)
cv.imshow("Campos de trigo color", imgBase)
plt.hist(imgBase.ravel(), 256, [0, 256])
plt.show()
cv.waitKey(0)
cv.destroyAllWindows()



